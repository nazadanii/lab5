from flask import Flask, jsonify
import os
import sqlite3
import mysql.connector

from db import setup_sqlite_db, setup_mysql_db

group_name = os.getenv("DATABASE_NAME", 'KB_3')
env = os.getenv("ENVIRONMENT", 'dev')

if env == 'dev':
    setup_sqlite_db(db_name=group_name)
else:
    db_user = os.getenv("db_user")
    db_password = os.getenv("db_password")
    db_hostname = os.getenv("db_hostname")
    db_port = os.getenv("db_port", "3306")
    setup_mysql_db(db_name=group_name, user=db_user, password=db_password, hostname=db_hostname, port=db_port)
app = Flask(__name__)


@app.route('/hello/<name>')
def hello(name):
    return jsonify({"message": f"Hello {name}"})

@app.route('/students', methods=['GET'])
def all_students():
    if env == 'dev':
        connector = get_db_connector(group_name=group_name)
    else:
        connector = get_db_connector(db_name=group_name, user=db_user, password=db_password, hostname=db_hostname, port=db_port)
    mycursor = connector.cursor()
    mycursor.execute("SELECT * FROM Students")
    myresult = mycursor.fetchall()
    all_students = {}
    for i in myresult:
        student = {"first_name": i[1], "middle_name": i[2], "second_name": i[3], "email": i[4]}
        all_students[i[0]] = student
    return jsonify(all_students)

@app.route('/students/<id>', methods=['GET'])
def student_by_id(id):
    if env == 'dev':
        connector = get_db_connector(group_name=group_name)
    else:
        connector = get_db_connector(db_name=group_name, user=db_user, password=db_password, hostname=db_hostname, port=db_port)
    mycursor = connector.cursor()
    mycursor.execute(f"SELECT * FROM Students WHERE id = {id}")
    myresult = mycursor.fetchone()
    return jsonify(
        {"first_name": myresult[1],
         "middle_name": myresult[2], 
         "second_name": myresult[3], 
         "email": myresult[4]}) if myresult else jsonify({})

def get_db_connector(**kwargs):
    config = dict(kwargs.items())
    if env == 'dev':
        return sqlite3.connect(f"{config.get('group_name')}.db")
    else:
        return mysql.connector.connect(user=config.get('user'), password=config.get('password'),
                                  host=config.get('hostname'),
                                  port=config.get('port'),
                                  database=config.get('db_name'),
                                  )

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=5000)